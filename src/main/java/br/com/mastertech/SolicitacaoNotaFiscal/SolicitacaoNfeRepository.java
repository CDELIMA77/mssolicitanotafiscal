package br.com.mastertech.SolicitacaoNotaFiscal;

import br.com.mastertech.SolicitacaoNotaFiscal.models.SolicitacaoNfe;
import org.springframework.data.repository.CrudRepository;

public interface SolicitacaoNfeRepository extends CrudRepository<SolicitacaoNfe, Long> {
    Iterable<SolicitacaoNfe> findByIdentidade(String identidade);
}
