package br.com.mastertech.SolicitacaoNotaFiscal;

import br.com.mastertech.SolicitacaoNotaFiscal.models.Log;
import br.com.mastertech.SolicitacaoNotaFiscal.models.SolicitacaoNfe;
import br.com.mastertech.SolicitacaoNotaFiscal.models.StatusDTO;
import br.com.mastertech.SolicitacaoNotaFiscal.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

@RestController
public class SolicitaNfeController {

    @Autowired
    private KafkaTemplate<String, SolicitacaoNfe> producer;

    @Autowired
    private KafkaTemplate<String, Log> producerlog;

    @Autowired
    SolicitaNfeService solicitaNfeService;

    @PostMapping("/nfe/emitir")
    public ResponseEntity<StatusDTO>  create(@RequestBody SolicitacaoNfe solicitacaoNfe, @AuthenticationPrincipal Usuario usuario) {
        Date data = new Date();
        Log log = new Log();
        log.setLog("[" + data + "]" + "[Emissão]: " + solicitacaoNfe.getIdentidade() + " acaba de pedir a emissão de uma NF no valor de R$ " + String.format("%.2f",  solicitacaoNfe.getValor())
        + " !");
        solicitacaoNfe.setStatus("Pending");
        StatusDTO statusDTO = new StatusDTO();
        statusDTO.setStatus("Pending");
        SolicitacaoNfe solicitacao = solicitaNfeService.salvar(solicitacaoNfe);
        producer.send("spec2-cynthia-carvalho-1", solicitacaoNfe);
        producerlog.send("spec2-cynthia-carvalho-2", log);
        return ResponseEntity.status(201).body(statusDTO);
    }

    @GetMapping("nfe/consultar/{identidade}")
    public ResponseEntity<Iterable<SolicitacaoNfe>> buscarSolicitacao(@PathVariable String identidade, @AuthenticationPrincipal Usuario usuario){
        Iterable<SolicitacaoNfe> solicitacao = solicitaNfeService.buscarPorIdentidade(identidade);
        if (solicitacao != null ){
            Date data = new Date();
            Log log = new Log();
            log.setLog("[" + data + "]" + "[Consulta]: " + identidade + " acaba de pedir os dados das suas notas fiscais.");
            producerlog.send("spec2-cynthia-carvalho-2", log);
            return ResponseEntity.status(200).body(solicitacao);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/nfe/atualizar")
    public void atualizar(@RequestBody SolicitacaoNfe solicitacaoNfe) {
        solicitaNfeService.atualizar(solicitacaoNfe);
    }

}
