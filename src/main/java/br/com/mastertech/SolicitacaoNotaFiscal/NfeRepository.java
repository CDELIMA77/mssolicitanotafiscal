package br.com.mastertech.SolicitacaoNotaFiscal;

import br.com.mastertech.SolicitacaoNotaFiscal.models.Nfe;
import org.springframework.data.repository.CrudRepository;

public interface NfeRepository extends CrudRepository<Nfe, Long> {
}
