package br.com.mastertech.SolicitacaoNotaFiscal;

import br.com.mastertech.SolicitacaoNotaFiscal.models.Nfe;
import br.com.mastertech.SolicitacaoNotaFiscal.models.SolicitacaoNfe;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SolicitaNfeService {

    @Autowired
    private SolicitacaoNfeRepository solicitacaoNfeRepository;

    @Autowired
    private NfeRepository nfeRepository;

    public Iterable<SolicitacaoNfe> buscarPorIdentidade(String identidade) {
        Iterable<SolicitacaoNfe> solicitacao = solicitacaoNfeRepository.findByIdentidade(identidade);
        return solicitacao;
    }

    public Optional<SolicitacaoNfe> buscarPorId(Long id) {
        Optional<SolicitacaoNfe> solicitacao = solicitacaoNfeRepository.findById(id);
        return solicitacao;
    }

    public SolicitacaoNfe salvar(SolicitacaoNfe solicitacaoNfe) {
        Nfe nfe = new Nfe();
        nfe.setValorInicial(solicitacaoNfe.getValor());
        nfe.setValorIRRF(0.00);
        nfe.setValorCSLL(0.00);
        nfe.setValorCofins(0.00);
        nfe.setValorFinal(solicitacaoNfe.getValor());
        Nfe nfeObjeto2 = nfeRepository.save(nfe);
        solicitacaoNfe.setNfe(nfeObjeto2);
        SolicitacaoNfe nfeObjeto1 = solicitacaoNfeRepository.save(solicitacaoNfe);
        return nfeObjeto1;
    }

    public void atualizar(SolicitacaoNfe solicitacaoNfe) {
        Optional<SolicitacaoNfe> solicitacao = buscarPorId(solicitacaoNfe.getId());
        if (solicitacao.isPresent()) {
            SolicitacaoNfe solicitacaoObjeto = solicitacao.get();
            solicitacaoObjeto.setStatus("Complete");
            solicitacaoObjeto.setNfe(solicitacaoNfe.getNfe());
            solicitacaoNfeRepository.save(solicitacaoObjeto);
        }
    }
}