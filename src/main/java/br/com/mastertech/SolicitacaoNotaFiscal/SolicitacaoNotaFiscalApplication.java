package br.com.mastertech.SolicitacaoNotaFiscal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SolicitacaoNotaFiscalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolicitacaoNotaFiscalApplication.class, args);
	}

}
