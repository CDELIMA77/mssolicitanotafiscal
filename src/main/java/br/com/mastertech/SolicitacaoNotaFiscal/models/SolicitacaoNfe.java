package br.com.mastertech.SolicitacaoNotaFiscal.models;

import javax.persistence.*;

@Entity
public class SolicitacaoNfe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String identidade;
    private Double valor; /* Double academico ok, porém vida real usar BigDecimal*/
    private String status;
    @OneToOne
    private Nfe nfe;

    public SolicitacaoNfe() {
    }

    public SolicitacaoNfe(Long id, String identidade, Double valor, String status, Nfe nfe) {
        this.id = id;
        this.identidade = identidade;
        this.valor = valor;
        this.status = status;
        this.nfe = nfe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Nfe getNfe() {
        return nfe;
    }

    public void setNfe(Nfe nfe) {
        this.nfe = nfe;
    }
}
